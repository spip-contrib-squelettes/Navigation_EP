<?php
/**
 * Squelette navigation_ep
 * (c) 2023 Licence GPL 3
 */

if (!defined("_ECRIRE_INC_VERSION")) return;

function navigation_ep_header_prive($flux){
	$flux .= "<!--navigation_ep css et script-->
	<link rel=\"stylesheet\" href=\"/plugins/auto/navigation_ep/v2.1.1/css/tailwind.css\" type=\"text/css\">
	<style>
    @media (min-width:1024px){
		body {
			padding-top:120px !important;
		}
    #page h1{
      padding-bottom:1em;
    }
  }
  @media (max-width:1024px){
		body {
			padding-top:60px !important;
		}
  }
    #page h1{
      font-weight:bold;
      font-variant: small-caps;
      font-size:1.3em;
    }
	</style>
	<!--fin insertion navigation_ep-->
	";
	return $flux;
}

function upper($chaine) {
	return $chaine ? strtoupper($chaine) : '';
}

/**
 * Increases or decreases the brightness of a color by a percentage of the current brightness.
 *
 * param   string  $hexCode        Supported formats: `#FFF`, `#FFFFFF`, `FFF`, `FFFFFF`
 * param   float   $adjustPercent  A number between -1 and 1. E.g. 0.3 = 30% lighter; -0.4 = 40% darker.
 *
 * return  string
 */
function adjustBrightness($hexCode, $adjustPercent) {
    $hexCode = ltrim($hexCode, '#');

    if (strlen($hexCode) == 3) {
        $hexCode = $hexCode[0] . $hexCode[0] . $hexCode[1] . $hexCode[1] . $hexCode[2] . $hexCode[2];
    }

    $hexCode = array_map('hexdec', str_split($hexCode, 2));

    foreach ($hexCode as & $color) {
        $adjustableLimit = $adjustPercent < 0 ? $color : 255 - $color;
        $adjustAmount = ceil($adjustableLimit * $adjustPercent);

        $color = str_pad(dechex($color + $adjustAmount), 2, '0', STR_PAD_LEFT);
    }

    return '#' . implode($hexCode);
}

function hex2rgba($color, $opacity = false)
{
    $default = 'rgb(0,0,0)';
    if (empty($color))
        return $default;
    if ($color[0] == '#') {
        $color = substr($color, 1);
    }

    if (strlen($color) == 6) {
        $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
    } elseif (strlen($color) == 3) {
        $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
    } else {
        return $default;
    }

    $rgb = array_map('hexdec', $hex);
    if ($opacity) {
        if (abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
    } else {
        $output = 'rgb(' . implode(",", $rgb) . ')';
    }

    return $output;
}

?>
